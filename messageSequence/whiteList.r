##########################################################################################################
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: whitelist pgm
#
# created by : adiel.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# created on : 2017-09-26
# updated on : 2017-11-13
#
# Copyright AKTANA (c) 2017.
#
##########################################################################################################
library(RMySQL)
library(data.table)
library(openxlsx)
library(properties)
library(futile.logger)

#################################################
# get all account IDs if WhiteListType is not set
#################################################
getAllAccountIDs <- function (con, learningRunUID, learningBuildUID, productUID)
{
  if (productUID == 'AKT_ALL_PRODUCTS') {
      query <- sprintf("SELECT DISTINCT act.accountId as 'accountId', act.externalId as 'objectUID', '%s' as 'learningRunUID', 
                              '%s' as 'learningBuildUID', 'AccountWhitelist' as 'listType' , 'Account' as 'objectType' 
                       FROM Account act INNER JOIN RepAccountAssignment raa ON act.accountId = raa.accountId
                       WHERE raa.accountId in (select distinct(accountId) from AccountProduct);", learningRunUID, learningBuildUID)    
  }
  else {
      productId <- dbGetQuery(con, sprintf("SELECT productId FROM Product WHERE externalId = '%s'", productUID))
  
      query <- sprintf("SELECT DISTINCT act.accountId as 'accountId', act.externalId as 'objectUID', '%s' as 'learningRunUID', 
                              '%s' as 'learningBuildUID', 'AccountWhitelist' as 'listType' , 'Account' as 'objectType' 
                       FROM Account act INNER JOIN RepAccountAssignment raa ON act.accountId = raa.accountId
                       WHERE raa.accountId in (select distinct(accountId) from AccountProduct where productId=%s);", learningRunUID, learningBuildUID, productId)
  }
  
  flog.info("getAllAccountIDs: query is: %s", query)

  accountAllIDs <- data.table(dbGetQuery(con, query))
  return(accountAllIDs);
}

#########################################
# get account list based on DSEConfig
#########################################
getAccountsByDSEConfig <- function(con, learningRunUID, learningBuildUID, commaDelimDSEConfigList, productUID)
{

  if (productUID == 'AKT_ALL_PRODUCTS') {
      query <- sprintf("SELECT DISTINCT act.accountId as 'accountId',  act.externalId as 'objectUID', '%s' as 'learningRunUID', 
                          '%s' as 'learningBuildUID', 'AccountWhitelist' as 'listType' , 'Account' as 'objectType'
                    FROM Account act INNER JOIN  RepAccountAssignment raa
                          INNER JOIN Rep r INNER JOIN AccountProduct ap
                    ON act.accountId = raa.accountId and raa.repId = r.repId and ap.accountId = raa.accountId 
                    WHERE find_in_set(r.seConfigId, '%s') ", learningRunUID, learningBuildUID, commaDelimDSEConfigList)    
  }
  else {  
      productId <- dbGetQuery(con, sprintf("SELECT productId FROM Product WHERE externalId = '%s'", productUID))

      query <- sprintf("SELECT DISTINCT act.accountId as 'accountId',  act.externalId as 'objectUID', '%s' as 'learningRunUID', 
                          '%s' as 'learningBuildUID', 'AccountWhitelist' as 'listType' , 'Account' as 'objectType'
                    FROM Account act INNER JOIN  RepAccountAssignment raa
                          INNER JOIN Rep r INNER JOIN AccountProduct ap
                    ON act.accountId = raa.accountId and raa.repId = r.repId and ap.accountId = raa.accountId 
                    WHERE ap.productId=%s and find_in_set(r.seConfigId, '%s') ", learningRunUID, learningBuildUID, productId, commaDelimDSEConfigList) 
  }

  flog.info("getAccountsByDSEConfig: query is: %s", query)

  accountWhiteListDB <- data.table(dbGetQuery(con,query))
  return(accountWhiteListDB);
}

#########################################
# get account list based on rep list
#########################################
getAccountsByRepList <- function(con, learningRunUID, learningBuildUID, commaDelimRepUIDList)
{
  query <- paste("SELECT DISTINCT act.accountId as 'accountId', act.externalId as 'objectUID', '",learningRunUID,"' as 'learningRunUID', 
                 '",learningBuildUID,"' as 'learningBuildUID', 'AccountWhitelist' as 'listType' , 'Account' as 'objectType' 
                 FROM Account act INNER JOIN RepAccountAssignment raa INNER JOIN Rep r 
                 ON act.accountId = raa.accountId AND raa.repId = r.repId 
                 WHERE  raa.startDate <= CURDATE()  AND  raa.endDate >= CURDATE() 
                 AND FIND_IN_SET(r.externalId, '", commaDelimRepUIDList,"') > 0;", sep="")
  
  flog.info("getAccountsByRepList: query is: %s", query)

  accountWhiteListDB <- data.table(dbGetQuery(con,query))
  return(accountWhiteListDB);
}

#########################################
# get account list based on type of list
########################################
getAccountsByListType <- function(con, learningRunUID, learningBuildUID, listType, commaDelimUIDList, productUID)
{  
  switch (
    listType,
    DSEConfig = whiteListDB <- getAccountsByDSEConfig(con, learningRunUID, learningBuildUID, commaDelimUIDList, productUID),
    Reps = whiteListDB <- getAccountsByRepList(con, learningRunUID, learningBuildUID, commaDelimUIDList),
    whiteListDB <- getAllAccountIDs(con, learningRunUID, learningBuildUID, productUID)
  )

  return(whiteListDB);
}

###########################################################
# whiteList main function
# Input: RUN_UID, BUILD_UID, CONFIG_UID, dbname, homedir
# return: whitListDB
###########################################################
whiteList <- function(con, con_l, RUN_UID, BUILD_UID, CONFIG_UID, dbname, homedir)
{

  learningRunUID <- RUN_UID
  learningBuildUID <- BUILD_UID
  learningConfigUID <- CONFIG_UID

  learningBuildsDir <- sprintf("%s/builds", homedir)
        
  if(!exists("learningRunUID")) {
    flog.error('Error: learningRunUID is required', name='error')
    quit(save = "no", status = 1, runLast = FALSE)
  }

  # if learning DB was not defined - use convention
  if(!exists("learningdbname")) {
    learningdbname = sprintf("%s_learning",dbname)
  }

  # if no learningBuildUID and no learningConfigUID, report error and quit.
  if(!exists("learningBuildUID") & !exists("learningConfigUID")) {
    flog.error('Error: either learningConfigUID or learningBuildUID  is required', name='error')
    quit(save = "no", status = 70, runLast = FALSE) # user-defined error code 70 for whiteListDB error
  }

  # nightly run will iterate over all published configs
  # otherwise, if not nightly run, expect a buildUID
  isNightly <- FALSE  # initialize to FALSE

  # if no BUILD_UID, then isNightly True; otherwise False
  if(!exists("BUILD_UID")) isNightly <- TRUE
      
  # get properties 
  if(isNightly) {
    propertiesFilePath <- sprintf("%s/nightly/%s/learning.properties",learningBuildsDir, learningConfigUID);
  }  else {
    propertiesFilePath <- sprintf("%s/%s/learning.properties",learningBuildsDir, learningBuildUID)
  }
  
  myProps <- list()
  productUID <- "AKT_ALL_PRODUCTS"  # set to default first
  
  if (file.exists(propertiesFilePath)) {
    myProps <- read.properties(propertiesFilePath)
    productUID <- myProps$productUID
  }
  else {
    print("Warning: The learning.properties for the following build and config doesn't exist!") 
    print(propertiesFilePath)    
  }

  # check that whitelist properties were set
  if(is.null(myProps$LE_MS_WhiteListType) || is.null(myProps$LE_MS_WhiteList)) {
    flog.info('No whiteList provided. Use all account instead.')
    myProps$LE_MS_WhiteListType <- "ALL"
  }

  # replace semicolon with comma which is required by Mysql find-in-set function
  if(!is.null(myProps$LE_MS_WhiteListType)) {
    myProps$LE_MS_WhiteListType <- gsub(";", ",", myProps$LE_MS_WhiteListType)
  }

  if(!is.null(myProps$LE_MS_WhiteList)) {
    myProps$LE_MS_WhiteList <- gsub(";", ",", myProps$LE_MS_WhiteList)
  }

  #### get white list ####
  whiteListDB <- getAccountsByListType(con, learningRunUID, learningBuildUID, myProps$LE_MS_WhiteListType, myProps$LE_MS_WhiteList, productUID)

  #### save white list ####
  if(exists("whiteListDB") && !is.null(whiteListDB)) {
  
    flog.info('WhiteList is printed into the print file for verification.')
    print("whiteListDB: ")
    print(whiteListDB)

    # copy table and remove accountId for use by dbWriteTable
    # we keep the full table for use by R module
    whiteListWithoutAccountIdDB <- copy(whiteListDB)
    whiteListWithoutAccountIdDB[,accountId:=NULL]

    nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    whiteListWithoutAccountIdDB$createdAt <- nowTime
    whiteListWithoutAccountIdDB$updatedAt <- nowTime

    FIELDS <- list(learningRunUID="varchar(80)", learningBuildUID="varchar(80)", listType="varchar(80)", objectType="varchar(80)", objectUID="varchar(80)", createdAt="datetime", updatedAt="datetime")

    # save into LearningObjectList table
    flog.info("WhiteListType is %s", myProps$LE_MS_WhiteListType)
  
    if (myProps$LE_MS_WhiteListType != "ALL") {
      tryCatch(dbWriteTable(con_l, "LearningObjectList", as.data.frame(whiteListWithoutAccountIdDB), append=T, row.names=F, overwrite=F, field.types=FIELDS), 
             error = function(e) {
               flog.error('Error in writing to LearningObjectList!', name='error')
             })
    }
    
    return (whiteListDB)
    
  } else {
    print(paste("Error: unsupported white list type '", myProps$LE_MS_WhiteListType,"'", sep=""))
    quit(save = "no", status = 1, runLast = FALSE)
  }

}

###########################################################
# whiteList main function (New Version)
# Input: con, con_l, RUN_UID, BUILD_UID, productUID, LE_MS_WhiteListType, LE_MS_WhiteList
# return: whitListDB
###########################################################
whiteListNew <- function(con, con_l, RUN_UID, BUILD_UID, productUID, LE_MS_WhiteListType, LE_MS_WhiteList)
{
  
  learningRunUID <- RUN_UID
  learningBuildUID <- BUILD_UID
  
  if(!exists("learningRunUID")) {
    flog.error('Error: learningRunUID is required', name='error')
    quit(save = "no", status = 1, runLast = FALSE)
  }
  
  # if no learningBuildUID and no learningConfigUID, report error and quit.
  if(is.null(learningBuildUID)) {
    flog.error('ErrorL learningBuildUID is required', name='error')
    quit(save = "no", status = 70, runLast = FALSE) # user-defined error code 70 for whiteListDB error
  }
  
  # check that whitelist properties were set
  if(is.null(LE_MS_WhiteListType) || is.null(LE_MS_WhiteList)) {
    flog.info('No whiteList provided. Use all account instead.')
    LE_MS_WhiteListType <- "ALL"
  }
  
  # replace semicolon with comma which is required by Mysql find-in-set function
  if(!is.null(LE_MS_WhiteListType)) {
    LE_MS_WhiteListType <- gsub(";", ",", LE_MS_WhiteListType)
  }
  
  if(!is.null(LE_MS_WhiteList)) {
    LE_MS_WhiteList <- gsub(";", ",", LE_MS_WhiteList)
  }
  
  #### get white list ####
  whiteListDB <- getAccountsByListType(con, learningRunUID, learningBuildUID, LE_MS_WhiteListType, LE_MS_WhiteList, productUID)
  
  #### save white list ####
  if(exists("whiteListDB") && !is.null(whiteListDB)) {
    
    flog.info('WhiteList is printed into the print file for verification.')
    print("whiteListDB: ")
    print(whiteListDB)
    
    # copy table and remove accountId for use by dbWriteTable
    # we keep the full table for use by R module
    whiteListWithoutAccountIdDB <- copy(whiteListDB)
    whiteListWithoutAccountIdDB[,accountId:=NULL]
    
    nowTime <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    whiteListWithoutAccountIdDB$createdAt <- nowTime
    whiteListWithoutAccountIdDB$updatedAt <- nowTime
    
    FIELDS <- list(learningRunUID="varchar(80)", learningBuildUID="varchar(80)", listType="varchar(80)", objectType="varchar(80)", objectUID="varchar(80)", createdAt="datetime", updatedAt="datetime")
    
    # save into LearningObjectList table
    flog.info("WhiteListType is %s", LE_MS_WhiteListType)
    
    if (LE_MS_WhiteListType != "ALL") {
      tryCatch(dbWriteTable(con_l, "LearningObjectList", as.data.frame(whiteListWithoutAccountIdDB), append=T, row.names=F, overwrite=F, field.types=FIELDS), 
               error = function(e) {
                 flog.error('Error in writing to LearningObjectList!', name='error')
               })
    }
    
    return (whiteListDB)
    
  } else {
    print(paste("Error: unsupported white list type '", LE_MS_WhiteListType,"'", sep=""))
    quit(save = "no", status = 1, runLast = FALSE)
  }
  
}
