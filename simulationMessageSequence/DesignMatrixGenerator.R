#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

library(RMySQL)
library(data.table)
library(h2o)
library(lattice)


  #setwd("~/code/INT-513/messageSequence/") 
  setwd("~/learning/messageSequence/") 
  
  h2o.init()
  #pName = "CHANTIX"
  #msgSet <- 1001
  homedir <- "~/learning"
  # load the read data and score and save code
  source(sprintf("%s/messageSequence/loadMessageSequenceData.R",homedir))
  dta <- loadMessageSequenceData()
  
  interactions <- dta[[1]]
  accountProduct <- dta[[2]]
  names(accountProduct) <- gsub("-","_",names(accountProduct))  # remove -'s from names
  messageSet <- dta[[4]]
  stableMessageNames <- gsub("-","_",dta[[5]])
  messages <- dta[[6]]
  
  stableMessageNames <- gsub("-","_",dta[[5]])
  sM <- accountProduct[productName==pName,stableMessageNames,with=F]
  sM$productName <- NULL
  tt <- sM[,colSums(is.na(sM))<nrow(sM)]
  sM <- sM[,tt,with=F]
  sM[is.na(sM)] <- 0
  t <- as.data.table(melt(sM,id.vars="accountId"))
  t <- t[value>0]
  t$ctr <- 1
  sM <- as.data.table(dcast(t,accountId~variable+value,fill=0))
  
  AP <- accountProduct[productName==pName]
  AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]
  tt<-data.table(sapply(AP,class))
  tt$names<-names(AP)
  chrV <- tt[V1=="character"]
  chrV <- chrV[names!="productName"]
  numV <- tt[V1!="character"]
  
  AP[is.na(AP)] <- "None"
  #AP <- data.table(model.matrix(~-1+.,AP))
  
  # now add the stable message stuff    
  AP<-merge(AP,sM,by="accountId")
  
  ints <- interactions[productName == pName,]
  
  setkey(ints,accountId,date)
  ints <- ints[productInteractionTypeName %in% c("RTE_CLICK","RTE_OPEN","SEND") & !is.na(physicalMessageUID)]
  ints[productInteractionTypeName=="RTE_OPEN",key:=paste0("OPEN...",physicalMessageUID)]
  ints[productInteractionTypeName=="RTE_CLICK",key:=paste0("CLIC...",physicalMessageUID)]
  ints[productInteractionTypeName=="SEND",key:=paste0("SEND...",physicalMessageUID)]
  
  msgs <- messageSet$stableMessageId[messageSet$messageSetId %in% msgSet]
  msgsPhy <- messages$lastPhysicalMessageUID[messages$messageId %in% msgs]
  targetNames <- paste0("OPEN...", msgsPhy)
  #flog.info("Number of targets: %s",length(targetNames))
  #for(nms in targetNames)flog.info("TargetNames = %s",nms) 
  
  t <- ints[,c("key","accountId"),with=F]
  t$ctr <- 1
  t <- as.data.table(dcast(t,accountId~key))
  m <- melt(t,id.vars="accountId")
  m[value>0,value:=1]
  t <- as.data.table(dcast(m,accountId~variable))
  t[is.na(t)] <- 0
  allModel <- merge(t,AP,by="accountId")
  saveAccountId <- allModel$accountId
  allModel$accountId <- NULL
  allModel.saved <- allModel
  # Trying different numbers of rows for all model
  allModel <- allModel.saved
  
  #t <- copy(allModel)
  #newt <- t[,c(grep("CLIC", grep("SEND", grep("OPEN", names(t), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE)), with = F]


buildDesignMatrix <- function(names){
  #names <- "a3RA0000000Mfu2MAC"
  t <- copy(allModel)
  newt <- t[,c(grep("CLIC", grep("SEND", grep("OPEN", names(t), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE)), with = F]
  targets <- unlist(strsplit(names, split = ","))
  for(k in 1:length(targets)){
    nme <- gsub("OPEN...", "", targets[k])
    newt$sendTarget <- 1
    newt$openTarget <- 1
    names(newt)[which(names(newt) %in% "sendTarget")] <- paste0("SEND...", nme)
    names(newt)[which(names(newt) %in% "openTarget")] <- paste0("OPEN...", nme)
  }
  return(newt)
}

buildInitialDesignMatrix <- function(msg){
  t <- copy(allModel)
  newt <- t[,c(grep("CLIC", grep("SEND", grep("OPEN", names(t), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE), value = TRUE, invert = TRUE)), with = F]
  return(newt)
}


generateDataMatrix <- function(newseq, outputDirectory){
  
  #outputDirectory <- "/Users/anwar/code/data/simulation/DataMatrix/300/%d"
  #newseq<- list('a3RA0000000U0UHMA0','a3RA0000000HO6iSRI','a3RA0000000HO6iCONSE2')
  for(m in 1:(length(newseq))){
    if(m == 1){
      msg <- newseq[1]
      dgn <- buildInitialDesignMatrix(msg)
      p.hex <- as.h2o(dgn)
      h2o.exportFile(p.hex, sprintf("%s/%s.data", outputDirectory, m))
      mdls <- newseq[m]
      #mdlPaths <- models$modelName[models$target %in% mdls]
      #initialpreds <- loadAndPredict(mdlPath = mdlPaths, newt = p.hex)
      #initialpreds$msgName <- NULL
     # names(initialpreds)[grep("p1", names(initialpreds))] <- newseq[1]
    }
    if(m > 1){
      names <- newseq[1:(m-1)]
      dgn <- buildDesignMatrix(names)
      p.hex <- as.h2o(dgn)
      h2o.exportFile(p.hex, sprintf("%s/%s.data",outputDirectory, m))
      mdls <- newseq[m]
      #mdlPaths <- models$modelName[models$target %in% mdls]
      #preds <- loadAndPredict(mdlPath = mdlPaths, newt = p.hex)
      #preds$msgName <- NULL
      #initialpreds <- merge(initialpreds, preds, by = "accountId")
      #initialpreds$product <- initialpreds[,m, with = F]*initialpreds[,m+1, with = F]
      #initialpreds$p1 <- NULL
      #names(initialpreds)[grep("p1", names(initialpreds))] <- newseq[m]
    }
  }
}

if(length(args)==0)
{
  print("No arguments supplied.")
} else 
{
  print("Arguments supplied.")
    for(i in 1:length(args))
    {
      print(args[i]);
    }
  message_sequence = as.list(strsplit(args[1], ","))[[1]]
  data_matrix_directory = args[2]
  generateDataMatrix(message_sequence, data_matrix_directory)
}