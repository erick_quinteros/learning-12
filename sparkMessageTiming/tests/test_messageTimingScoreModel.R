context('test the messageTimingScoreModel func in the sparktMessageTiming module')
print(Sys.time())

# load library, loading common source scripts
library(h2o)
library(uuid)
library(Learning)
library(sparkLearning)
library(sparklyr)
library(dplyr)

source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf("%s/sparkMessageTiming/messageTimingScoreModel.R",homedir))
source(sprintf("%s/sparkMessageTiming/tteReport.R",homedir))
source(sprintf("%s/sparkMessageTiming/tteMethods.R",homedir))
source(sprintf("%s/sparkMessageTiming/getTTEparams.R",homedir))
source(sprintf("%s/sparkMessageTiming/buildStaticFeatures.R",homedir))

libdir <<- sprintf("%s/library", homedir)

CATALOGENTRY <<- " "

# start h2o
print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="8g")),
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

############################### func for reset mock data for run scoreModel #######################################
runScoreModelMockDataReset <- function() {
  # writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account','MessageSet','TimeToEngageAlgorithm'),pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun','AccountTimeToEngageReport','SegmentTimeToEngageReport','TTEhcpReport','TTEsegmentReport','AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'),pfizerprod_cs=c('Approved_Document_vod__c','RecordType', 'Call2_Sample_vod__c'))
  # 'MessageSet','MessageAlgorithm','MessageSetMessage' is for adding necessary param to run scoreModel

  resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

  # set up unit test build folder and related configs
  # get buildUID
  buildUID <- readModuleConfig(homedir, 'messageTiming','buildUID')
  # add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
  setupMockBuildDir(homedir, 'messageTiming',buildUID, updateModelSaveFileSheetNo=list(2,4,6))
  return (buildUID)
}

########################### func for prepare arguments necessary for run scoreModel func and run ###################
# establish a func to run the setup and call scoreModel as it will be used multiple times for different cases
runScoreModel <- function(isNightly, buildUID) {
  # get arugments requried for logging & aruguments required for running the script (common for nightly and manual scoring)
  BUILD_UID <<- buildUID
  DRIVERMODULE <- "messageTimingScoreDriver.r"
  RUN_UID <<- UUIDgenerate()

  config <- initializeConfigurationNew(homedir, BUILD_UID)
  runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=NULL, createExcel=TRUE, createPlotDir=TRUE)
  runSettings[["modelsaveSpreadsheetName"]] <- sprintf('%s/builds/%s/messageTimingDriver.r_%s.xlsx',homedir,BUILD_UID,BUILD_UID)
  scoreDate <- as.Date(readModuleConfig(homedir, 'messageTiming','scoreDate')) # fix scoreDate so the result is always the same

  con   <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
  con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname, port)

  sc <<- spark_connect(master="local", version="2.3.1")

  # load saved whiteList from messageSequence module
  load(sprintf('%s/messageSequence/tests/data/from_whiteList_whiteListDB.RData', homedir)) # whiteListDB

  # load result data from messageTimingData
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_interactions.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accountProduct.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_products.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accounts.RData', homedir))

  interactions[, date := as.character(date)]  # for conversion in spark
  interactions <- sdf_copy_to(sc, interactions, "interactions", overwrite=TRUE, memory=FALSE)
  interactions <<- interactions %>% dplyr::mutate(date = to_date(date))

  accountProduct <<- sdf_copy_to(sc, accountProduct, "accountProduct", overwrite=TRUE, memory=FALSE)
  products <<- sdf_copy_to(sc, products, "products", overwrite=TRUE, memory=FALSE)
  accounts <<- sdf_copy_to(sc, accounts, "accounts", overwrite=TRUE, memory=FALSE)

  # get arguments for manually and nightly run separately & run scirpt

  productUID <- getConfigurationValueNew(config, "productUID")
  pName <- products[externalId==productUID]$productName

  modelsToScore <- data.table(dbGetQuery(con,"Select tteAlgorithmId, externalId, tteAlgorithmName from TimeToEngageAlgorithm where isDeleted=0 and isActive=1"))

  tteAlgoId <- unique(modelsToScore$tteAlgorithmId)[1]
  tteAlgorithmName <- modelsToScore[tteAlgorithmId==tteAlgoId]$tteAlgorithmName

  # run script
  runDir <- runSettings[["runDir"]]
  modelsaveDir <- runSettings[["modelsaveDir"]]
  modelsaveSpreadsheetName <- runSettings[["modelsaveSpreadsheetName"]]

  tteParams <- getTTEparams(config, "SCORE")
  tteParams[["tteAlgorithmName"]] <- tteAlgorithmName

  result <- messageTimingScoreModel(sc, con, con_l, runSettings, tteParams, whiteListDB)

  # db disconnect
  dbDisconnect(con)
  dbDisconnect(con_l)
  # final clean up
  closeClient()

  return(list(RUN_UID, result))
}

################################### main test procedures and cases ######################################################
test_that("tests for manual scoring", {
  # arguments for manual scoring
  isNightly <- FALSE
  runmodel <<- " "

  buildUID <- runScoreModelMockDataReset()

  # Run scoring
  res <- runScoreModel(isNightly, buildUID)

  result_new <- res[[2]]
  runUID <- res[[1]]

  # test cases

  # check calculation result is the same (as always use the same saved model and data)
  scores.file <- sprintf('%s/messageTiming/tests/data/from_TTE_manual_scores.RData', homedir)
  print(scores.file)
  load(scores.file)
  scores <- t

  scores$learningRunUID <- NULL
  scores_new <- result_new[[2]]

  scores_new <- data.table(collect(scores_new))

  scores_new$learningRunUID <- NULL

  expect_equal(result_new[[1]],0)
  expect_equal(dim(scores_new), c(18540, 5))

  # check build dir structure
  expect_file_exists(sprintf('%s/builds/%s',homedir,buildUID))
  expect_file_exists(sprintf('%s/builds/%s/log_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/print_%s.txt',homedir,buildUID,paste("Score",runUID,sep="_")))
  expect_file_exists(sprintf('%s/builds/%s/plot_output_%s',homedir,buildUID,paste("Score",runUID,sep="_")))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))
  # check general log information
  ind <- grep('Predict model outcomes', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Return from messageTimingScoreModel', log_contents)
  expect_gt(ind, 0)

})

test_that("tests for nightly scoring", {
  # arguments for manual scoring
  isNightly <- TRUE
  runmodel <<- " "

  buildUID <- runScoreModelMockDataReset()

  res <- runScoreModel(isNightly, buildUID)

  runUID <- res[[1]]
  result_new <- res[[2]]

  scores.file <- sprintf('%s/messageTiming/tests/data/from_TTE_nightly_scores.RData', homedir)
  print(scores.file)
  load(scores.file)
  scores <- t

  scores$learningRunUID <- NULL
  scores_new <- result_new[[2]]

  scores_new <- data.table(collect(scores_new))

  scores_new$learningRunUID <- NULL

  expect_equal(result_new[[1]], 0)
  expect_equal(dim(scores_new), c(18540, 5))

  # run tests on log file contents
  # read log files for info (func in source(sprintf('%s/common/unitTest/utilsFunc.r',homedir)))
  log_contents <- readLogFile(homedir,buildUID,paste("Score",runUID,sep="_"))

  # check general log information
  ind <- grep('Predict model outcomes', log_contents)
  expect_gt(ind, 0)

  ind <- grep('Return from messageTimingScoreModel', log_contents)
  expect_gt(ind, 0)

})
