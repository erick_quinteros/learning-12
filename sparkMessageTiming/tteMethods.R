##########################################################
#
#
# aktana- messageTiming Scoring method module.
#
# Description: messageTiming Scoring method 
#
# created by : wendong.zhu@aktana.com
#
# created on : 2018-07-05
#
# Copyright AKTANA (c) 2018.
#
#
####################################################################################################################

library(h2o)
library(lattice)
library(openxlsx)
library(data.table)
library(Hmisc)
library(sparkLearning)
library(sparklyr)
library(dplyr)

DAYS_PREDICT  <- 14   # 1 - defaul today; # 2 - today & tomorrow

# TTE methods:
#   A - Covariates + Intervals + DoW (full model for DSE integration)
#   B - Covariates + Intervals (for reporting)
#   C - Covariates + DoW (for reporting)

##
## Function of Method: Covariates + Time Interval between Visits & Sends (+ Dow)
##
tteMethod <- function(sc, models, AP, ints, RUN_UID, method)
{
    # Construct Design Matrix for prediction
    AP <- AP %>% collect() %>% data.table()
    setorder(AP, accountId)

    if (ncol(AP) > 1)
      allModel <- merge(AP, ints, by="accountId")
    else
      allModel <- ints

    rm(AP)
    gc()
    
    savedAccountIds <- allModel$accountId
    savedDates <- allModel$date
    savedEvents <- allModel$event
    
    if (method == "B") {
        savedInterval <- allModel$intervalOrDoW  # save interval in method B
    }
    
    if (method == "A" || method == "C") {  # dow is not in reporting (B), but in (A, C)
        allModel[, dow := weekdays(date, abbreviate=T)] 
    }

    allModel[, c("accountId", "date") := NULL]
    
    if (method == "B") {
        allModel[, c("intervalOrDoW") := NULL]
    }
    
    ## Load saved model
    modelPath <- models[1]$modelName
    rf.hex <- h2o.loadModel(path=modelPath)

    flog.info("Predict model outcomes")
    flog.info("Number of unique design matrix (method %s) records: %s", method, dim(allModel)[1])

    # convert allModel to h2o object
    fwrite(allModel, "/tmp/allModel.csv", col.names=FALSE, buffMB = 128L, nThread = getDTthreads())
    flog.info("Done writing allModel into csv file.")
    gc(); gc(); gc()

    p.hex = h2o.importFile(path="/tmp/allModel.csv", destination_frame="p.hex", header=FALSE, col.names=names(allModel))

    rm(allModel)
    rm(ints)
    gc(); gc(); gc()

    flog.info("Check p.hex rows : %s", dim(p.hex)[1])
    
    # Make Predictions
    scores <- h2o.predict(rf.hex, p.hex)  

    rm(p.hex)
    gc(); gc(); gc()

    scores <- as.data.table(scores)  # convert to data.table object
    
    scores$accountId <- savedAccountIds
    scores$date <- as.character(savedDates)
    scores$event <- savedEvents
    scores$learningRunUID <- RUN_UID
    
    if (method == "B") {
        scores$intervalOrDoW <- savedInterval
    }
    
    return(scores)
}
