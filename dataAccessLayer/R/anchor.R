#########################################################
# anchor register output tables
#########################################################
dataAccessLayer.anchor.registerOutputTables <- function() {
  anchorOutputTables <<- list( RepDateLocation_nightly = list(schema="dse", tableName="RepDateLocation"),
                               RepDateLocation_manual = list(schema="learning", tableName="RepDateLocation"),
                               RepDateFacility_nightly = list(schema="dse", tableName="RepDateFacility"),
                               RepDateFacility_manual = list(schema="learning", tableName="RepDateFacility"),
                               AccountDateLocation_nightly = list(schema="dse", tableName="AccountDateLocation"),
                               AccountDateLocation_manual = list(schema="learning", tableName="AccountDateLocation"),
                               AccountDateLocationScores = list(schema="learning", tableName="AccountDateLocationScores"),
                               RepCalendarAdherence = list(schema="learning", tableName="RepCalendarAdherence"),
                               RepAccountCalendarAdherence = list(schema="learning", tableName="RepAccountCalendarAdherence")
                              )
  return(anchorOutputTables)
}

#########################################################
# anchor common functions
#########################################################

dataAccessLayer.anchor.getTableNameFromTableMap <- function(tableNameKey) {
  tableName <- dataAccessLayer.common.getTableNameFromTableMap(anchorOutputTables, tableNameKey)
  return(tableName)
}

dataAccessLayer.anchor.getSchemaFromTableMap <- function(tableNameKey) {
  schema <- dataAccessLayer.common.getSchemaFromTableMap(anchorOutputTables, tableNameKey)
  return(schema)
}

dataAccessLayer.anchor.truncate <- function(tableNameKey) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.truncate(schema, tableName)
}

dataAccessLayer.anchor.deleteFrom <- function(tableNameKey, strFilterKeyValuePair) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.deleteFrom(schema, tableName, strFilterKeyValuePair)
}

dataAccessLayer.anchor.write <- function(tableNameKey, dataToWrite) {
  schema <- dataAccessLayer.anchor.getSchemaFromTableMap(tableNameKey)
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap(tableNameKey)
  dataAccessLayer.common.write(schema, tableName, dataToWrite)
}


#########################################################
# Custom data accesses for anchor module
#########################################################

dataAccessLayer.anchor.get.interactionTypeIds <- function() {
  return(dbGetQuery(con, "SELECT interactionTypeId FROM InteractionType WHERE interactionTypeName LIKE \"%VISIT\";")[["interactionTypeId"]])
}

dataAccessLayer.anchor.get.interactions <- function(loadStartDate, loadEndDate, interactionTypeIds) {
  Q <- sprintf("SELECT v.repId, f.latitude, f.longitude, v.facilityId, v.startDateLocal, v.isCompleted, ia.accountId, v.startDateTime, f.timeZoneId FROM Interaction as v JOIN Facility f ON v.startDateLocal >= '%s' AND v.startDateLocal <= '%s' AND v.interactionTypeId in (%s) AND v.isDeleted=0 AND v.facilityId=f.facilityId AND f.latitude IS NOT NULL AND f.longitude IS NOT NULL AND v.repId IN (SELECT repId FROM Rep r WHERE r.seConfigId>0) JOIN InteractionAccount ia ON ia.interactionId=v.interactionId;",loadStartDate, loadEndDate, paste(interactionTypeIds,collapse=","))
  return(dbGetQuery(con,Q))
}

dataAccessLayer.anchor.get.validReps <- function() {
  return(dbGetQuery(con, "SELECT repId, externalId as repUID FROM Rep r WHERE r.seConfigId>0;"))
}

dataAccessLayer.anchor.get.accounts <- function() {
  return(dbGetQuery(con, "SELECT accountId, externalId as accountUID FROM Account"))
}

dataAccessLayer.anchor.get.repAccountAssignments <- function(loadPredictRundate, validRepIds) {
  Q <- sprintf("SELECT DISTINCT repId, accountId FROM RepAccountAssignment_arc WHERE createdAt<='%s' AND arcendDate>'%s' AND repId IN (%s);", loadPredictRundate, loadPredictRundate, paste(validRepIds, collapse=","))
  return(repAccountAssignments <- dbGetQuery(con_stage, Q))
}

dataAccessLayer.anchor.get.repTeamRep <- function() {
  SQL <- sprintf("SELECT repTeamId, repId FROM RepTeamRep WHERE repId IN (SELECT repId FROM Rep r WHERE r.seConfigId>0);")
  return(dbGetQuery(con, SQL))
}

dataAccessLayer.anchor.get.adlScores <- function() {
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap("AccountDateLocationScores")
  SQL <- sprintf("SELECT learningRunUID, learningBuildUID, runDate FROM %s limit 1;", tableName)
  return(dbGetQuery(con_l,SQL))
}

dataAccessLayer.anchor.get.accountDateLocationScores <- function() {
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap("AccountDateLocationScores")
  SQL <- sprintf("SELECT accountId, facilityId, periodOfDay, dayOfWeek, latitude, longitude, facilityDoWScore, facilityPoDScore FROM %s;", tableName) 
  return(dbGetQuery(con_l,SQL))
}

dataAccessLayer.anchor.get.calAdherence <- function() {
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap("RepCalendarAdherence")
  SQL <- sprintf("SELECT learningRunUID, learningBuildUID, runDate FROM %s limit 1;", tableName)
  return(dbGetQuery(con_l,SQL))
}

dataAccessLayer.anchor.get.repCalendarAdherence <- function() {
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap("RepCalendarAdherence")
  SQL <- sprintf("SELECT repId, completeProb, completeConfidence, rescheduleProb, rescheduleConfidence FROM %s;", tableName) 
  return(dbGetQuery(con_l,SQL))
}

dataAccessLayer.anchor.get.repAccountCalendarAdherence <- function() {
  tableName <- dataAccessLayer.anchor.getTableNameFromTableMap("RepAccountCalendarAdherence")
  SQL <- sprintf("SELECT repId, accountId, completeProb, completeConfidence, rescheduleProb, rescheduleConfidence FROM %s;", tableName) 
  return(dbGetQuery(con_l,SQL))
}

dataAccessLayer.anchor.get.CallHistory <- function(startDate, predictRunDate) {
  # SQL <- sprintf("SELECT DISTINCT Call2_vod__c as interactionId, OwnerId as repUID, Account_vod__c as accountUID, LastModifiedDate, Status_vod__c as status, Call_Datetime_vod__c as callDatetime FROM STG_Call2_H WHERE createdDate<='%s' AND LastModifiedDate<='%s' AND Call_Date_vod__c<='%s' AND createdDate>='%s' AND LastModifiedDate>='%s' AND Call_Date_vod__c>='%s' AND Call_Type_vod__c in ('Detail Only','Call Only','Detail with Sample','Group Detail','Group Detail with Sample');", predictRunDate, predictRunDate, predictRunDate, startDate, startDate, startDate) 
  SQL <- sprintf("SELECT DISTINCT Call2_vod__c as interactionId, OwnerId as repUID, Account_vod__c as accountUID, LastModifiedDate, Status_vod__c as status, Call_Datetime_vod__c as callDatetime FROM STG_Call2_vod__c_H WHERE createdDate<='%s' AND LastModifiedDate<='%s' AND Call_Date_vod__c<='%s' AND createdDate>='%s' AND LastModifiedDate>='%s' AND Call_Date_vod__c>='%s' AND Call_Type_vod__c in ('Detail Only','Call Only','Detail with Sample','Group Detail','Group Detail with Sample');", predictRunDate, predictRunDate, predictRunDate, startDate, startDate, startDate) 
  return(dbGetQuery(con_archive,SQL))
}

dataAccessLayer.anchor.write.RepDateLocation_dse <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepDateLocation_nightly", dataToWrite)
}

dataAccessLayer.anchor.write.RepDateLocation_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepDateLocation_manual", dataToWrite)
}

dataAccessLayer.anchor.write.RepDateFacility_dse <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepDateFacility_nightly", dataToWrite)
}

dataAccessLayer.anchor.write.RepDateFacility_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepDateFacility_manual", dataToWrite)
}

dataAccessLayer.anchor.write.AccountDateLocationScores_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("AccountDateLocationScores", dataToWrite)
}

dataAccessLayer.anchor.write.AccountDateLocation_dse <- function(dataToWrite) {
  dataAccessLayer.anchor.write("AccountDateLocation_nightly", dataToWrite)
}

dataAccessLayer.anchor.write.AccountDateLocation_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("AccountDateLocation_manual", dataToWrite)
}

dataAccessLayer.anchor.write.RepCalendarAdherence_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepCalendarAdherence", dataToWrite)
}

dataAccessLayer.anchor.write.RepAccountCalendarAdherence_l <- function(dataToWrite) {
  dataAccessLayer.anchor.write("RepAccountCalendarAdherence", dataToWrite)
}

dataAccessLayer.anchor.deleteFrom.RepDateLocation <- function(BUILD_UID) {
  dataAccessLayer.anchor.deleteFrom("RepDateLocation_manual", list(list(key="learningBuildUID", value=BUILD_UID)))
}

dataAccessLayer.anchor.deleteFrom.RepDateFacility <- function(BUILD_UID) {
  dataAccessLayer.anchor.deleteFrom("RepDateFacility_manual", list(list(key="learningBuildUID", value=BUILD_UID)))
}

dataAccessLayer.anchor.deleteFrom.AccountDateLocation <- function(BUILD_UID, RUN_UID) {
  dataAccessLayer.anchor.deleteFrom("AccountDateLocation_manual", list(list(key="learningBuildUID", value=BUILD_UID), list(key="learningRunUID", value=RUN_UID))) # remove old build records in Learning DB
}

dataAccessLayer.anchor.truncate.RepDateLocation <- function() {
  dataAccessLayer.anchor.truncate("RepDateLocation_nightly")
}

dataAccessLayer.anchor.truncate.RepDateFacility <- function() {
  dataAccessLayer.anchor.truncate("RepDateFacility_nightly")
}

dataAccessLayer.anchor.truncate.AccountDateLocation <- function() {
  dataAccessLayer.anchor.truncate("AccountDateLocation_nightly")
}

dataAccessLayer.anchor.truncate.AccountDateLocationScores <- function() {
  dataAccessLayer.anchor.truncate("AccountDateLocationScores")
}

dataAccessLayer.anchor.truncate.RepCalendarAdherence <- function() {
  dataAccessLayer.anchor.truncate("RepCalendarAdherence")
}

dataAccessLayer.anchor.truncate.RepAccountCalendarAdherence <- function() {
  dataAccessLayer.anchor.truncate("RepAccountCalendarAdherence")
}

