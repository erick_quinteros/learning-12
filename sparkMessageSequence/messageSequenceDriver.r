##########################################################
#
#
# aktana- messageSequence estimates estimates Aktana Learning Engines.
#
# description: estimate rep likelihood of engaging
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-01
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
library(uuid)
library(h2o)
library(futile.logger)
library(Learning)

options(rgl.useNULL=TRUE)
Sys.setenv("R_ZIPCMD"="/usr/bin/zip")
print(sessionInfo())
#gcinfo(TRUE)  # print whenever GC triggeres

print("start h2o early")
tryCatch(suppressWarnings(h2o.init(nthreads=-1, max_mem_size="24g")), 
         error = function(e) {
           flog.error('Error in h2o.init()',name='error')
           quit(save = "no", status = 65, runLast = FALSE) # user-defined error code 65 for failure of h2o initialization
         })

# set parameters 
args <- commandArgs(T)

if(length(args)==0){
    print("No arguments supplied.")
    if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
        print("Arguments supplied.")
        for(i in 1:length(args)){
          eval(parse(text=args[[i]]))
          print(args[[i]]);
    }
}
# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}


# parameter defaults
DRIVERMODULE <- "messageSequenceDriver.r"
if(!exists("RUN_UID")) RUN_UID <- UUIDgenerate()

# replace cleanup function to save failure in learning DB
cleanUpMSOBuild <- function () 
{   tryCatch ({
    # update learningBuild table
    now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
    SQL <- sprintf("UPDATE LearningBuild SET executionStatus='failure', executionDateTime='%s' WHERE learningBuildUID='%s';",now,BUILD_UID)
    dbGetQuery(con_l,SQL)
    # update learningFile table
    logName <- runSettings[["logName"]]
    printName <- runSettings[["printName"]]
    blobIt <- function(name)
    {
      t0 <- readBin(name,what="raw",n=1e7)
      t1 <- paste0(t0,collapse="")
      return(t1)
    }
    outfile <- blobIt(logName)
    SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"log","txt",object.size(outfile),outfile)
    dbGetQuery(con_l,SQL)
    outfile <- blobIt(printName)
    SQL <- sprintf("INSERT INTO LearningFile(learningFileUID,learningRunUID,learningBuildUID,fileName,fileType,fileSize,data) VALUES('%s',NULL,'%s','%s','%s','%s','%s');",UUIDgenerate(),BUILD_UID,"print","txt",object.size(outfile),outfile)
    dbGetQuery(con_l,SQL)
    },
    error = function(e){flog.error("Running Error Handler failed: %s", e)},
    finally = {
      # disconnet DB and exit
      dbDisconnect(con)
      dbDisconnect(con_l)
      dbDisconnect(con_stage)
      dbDisconnect(con_cs)
      q('no',status=1)}
    )
    # exit()
}

# load the read data and estimation code
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf("%s/messageSequence/loadMessageSequenceData.R",homedir))
source(sprintf("%s/messageSequence/messageSequence.R",homedir))
source(sprintf("%s/messageSequence/saveBuildModel.R",homedir))
source(sprintf("%s/messageSequence/messageClustering.r",homedir))
source(sprintf("%s/sparkMessageSequence/messageSequenceHelper.R",homedir))


# get dbConnections
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
con_l <- getDBConnectionLearning(dbuser, dbpassword, dbhost, dbname_learning, port)
con_stage <- getDBConnectionStage(dbuser, dbpassword, dbhost, dbname_stage, port)
con_cs <- getDBConnectionCS(dbuser, dbpassword, dbhost, dbname_cs, port)

# now can proceed
# initialize the client
runSettings <- initializeClientNew(DRIVERMODULE, homedir, BUILD_UID, RUN_UID, errorFunc=cleanUpMSOBuild, createExcel=TRUE, createModelsaveDir=TRUE)
runSettings[["spreadsheetName"]] <- sprintf("%s/builds/%s/%s_%s.xlsx", homedir, BUILD_UID, DRIVERMODULE, BUILD_UID)
runSettings[["modelsaveDir"]] <- sprintf("%s/builds/%s/models_%s", homedir, BUILD_UID, BUILD_UID)
flog.info("Run initialized at: %s",Sys.time())

# run messageClustering
messageClustering(con_cs, con_l, con)

# get properties
config <- initializeConfigurationNew(homedir, BUILD_UID)
versionUID <- getConfigurationValueNew(config, "versionUID")
configUID <- getConfigurationValueNew(config, "configUID")
productUID <- getConfigurationValueNew(config, "productUID")
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")

# Check if the learning.properties mentions this model as an optimized model 
isOptimized <- getConfigurationValueNew(config, "LE_MS_isOptimizedVersion", as.logical, FALSE)
if(isOptimized)
{
  # Update the config to get the optimal parameters from database
  config <- updateLearningConfigForOptimalParameters(config, con_l)
}

# read the data
startTimer <- Sys.time()
dta <- loadMessageSequenceData(con, con_l, con_stage, con_cs, productUID, readDataList=c("interactions","accountProduct","products","messageSet","messageSetMessage","messages","emailTopicNames","messageTopic","interactionsP","expiredMessages","accountPredictorNames","emailTopicNameMap"))
for(i in 1:length(dta))assign(names(dta)[i],dta[[i]])

##### clean up memory
rm(dta)
#####
print("memory after data read and cleanup")
print(sapply(ls(), function(x) as.numeric(object.size(eval(parse(text=x))))))

if (! exists("targetNames")) { # for local test only, local test will give targetNames to just score for some of messages
  targetNames <- NULL
}

# estimate the models
startTimer <- Sys.time()
result <- messageSequence(config, runSettings[["modelsaveDir"]], interactions, accountProduct, products, emailTopicNames, interactionsP, targetNames)
flog.info("Model Build Time = %s",Sys.time()-startTimer)

# save outputs
if (nrow(result[["models"]]) <= 0) {
  flog.error("No models successfully built for this BUILD_UID")
  stop("No models successfully built for this BUILD_UID")
}
# save excel
saveBuildModelExcel(homedir, runSettings[["WORKBOOK"]], runSettings[["spreadsheetName"]], result[["models"]], result[["output"]], result[["APpredictors"]], result[["pName"]], messages, messageSet, messageSetMessage, messageTopic, expiredMessages, accountPredictorNames, emailTopicNameMap, result[["predictorNamesAPColMap"]])
# save to db
saveBuildModelDB(con_l, BUILD_UID, RUN_UID, runSettings[["logName"]], runSettings[["printName"]], runSettings[["spreadsheetName"]])

# shut down h2o
h2o.shutdown(prompt=FALSE)

# call deploy API on successful build
# initialize DEPLOY_ON_SUCCESS
if (!exists("deployOnSuccess")) {
  flog.info("deployOnSuccess not passed from API, set to FALSE")
  deployOnSuccess <- FALSE
} else {
  flog.info("deployOnSuccess passed from API, =%s", deployOnSuccess)
  deployOnSuccess <- as.logical(deployOnSuccess)
  if (is.null(deployOnSuccess)) {
    deployOnSuccess <- FALSE # set to false to non logical convertible value
    flog.info("deployOnSuccess set to False as not regonized as logical false")
  } else {
    flog.info("deplyOnSuccess set to %s as passed from API", deployOnSuccess)
  }
}
if (deployOnSuccess) {
  # source API call related scripts
  source(sprintf("%s/common/APICall/global.R",homedir))
  # initialize API call params
  apisecret <- data.table(dbGetQuery(con_l,"SELECT value FROM `sysParameters` WHERE name='apitokenSecret';"))$value[1]
  apiurl <- fromJSON(sprintf("%s/common/APICall/env.json", homedir))
  newAPIConfig <- list("learningApi"=apiurl,"secret"=apisecret, "username"="aktanaadmin", "password"="")
  init(TRUE, newAPIConfig)
  # Authenticate and get a token
  Authenticate(glblLearningAPIurl)
  # call deploy API
  req <- putAPIJson(glblLearningAPIurl, paste("LearningConfig/",configUID,"/versions/",versionUID,"/builds/",BUILD_UID,"/deploy",sep = ""))
}

#### need to update the executionDateTime ####
now <- format(Sys.time(), "%Y-%m-%d %H:%M:%S")
SQL <- sprintf("UPDATE LearningBuild SET executionStatus='%s', executionDateTime='%s' WHERE learningBuildUID='%s';","success",now,BUILD_UID)
dbGetQuery(con_l,SQL)

# disconnect dbs
dbDisconnect(con)
dbDisconnect(con_l)
dbDisconnect(con_stage)
dbDisconnect(con_cs)

# final clean up
closeClient()
