##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description:
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
#
##########################################################

initializeClient <- function()
{
    library(properties)
    
    CATALOGENTRY <<- " "

    options(error=cleanUp)

    timeStamp <<- RUN_UID # gsub(":| |-","_",sprintf("%s",Sys.time()))
    runStamp <<- BUILD_UID 
    runDir <<- sprintf("%s/builds/%s",homedir,BUILD_UID)
    
    plotDir <<- sprintf("%s/plot_output_%s_%s",runDir,paste("Build",runStamp,sep="_"),timeStamp)
    plotRef <<- sprintf("./plot_output_%s_%s",paste("Build",runStamp,sep="_"),timeStamp)
    dir.create(plotDir,showWarnings=T,recursive=T)

    catalogFile <<- sprintf("%s/builds/Catalog.txt",homedir)

    flog.appender(appender.console())
    flog.appender(appender.file(sprintf("%s/log_%s.txt",runDir,paste("Build",timeStamp,sep="_"))))

    flog.threshold(INFO)
    flog.info("Run directory = %s",runDir)

    # printed output
    sink(sprintf("%s/print_%s.txt",runDir,paste("Build", timeStamp, sep="_")))

    # workbook for output
    WORKBOOK <<- createWorkbook()
}
