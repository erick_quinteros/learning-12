##########################################################
#
#
# LearningPackage Func: initializeConfigurationNew()
#
# description: adopt from original initializeConfiguration()
#              read learning.properties file
#
# created by : shirley.xu@aktana.com
#
# created on : 2018-09-25
#
# Copyright AKTANA (c) 2018.
#
#
##########################################################

initializeConfigurationNew <- function(homedir, configFolderName, inNightlyFolder=FALSE)
{
  # get properties 
  if (!inNightlyFolder) {
    propertiesFilePath <- sprintf("%s/builds/%s/learning.properties",homedir,configFolderName)
  } else {
    propertiesFilePath <- sprintf("%s/builds/nightly/%s/learning.properties",homedir,configFolderName)
  }
  if (file.exists(propertiesFilePath)) {
    config <- read.properties(propertiesFilePath)
  } else {
    flog.error("The learning.properties for the following config doesn't exist!: %s", propertiesFilePath)
    config <- NULL
  }
  flog.info("Return from initConfiguration")
  return(config)
}