context('testing messageTimingData() func in TTE module')
print(Sys.time())

library(Learning)

# source script and code setup
# required to run tests
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))
source(sprintf("%s/messageTiming/getTTEparams.R",homedir))

# writing mock data to database (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
requiredMockDataList <- list(pfizerusdev=c('Product','RepTeam','Message','MessageSetMessage','Event','EventType','Interaction','InteractionType','ProductInteractionType','InteractionProduct','InteractionAccount','InteractionProductMessage','AccountProduct','Rep','RepTeamRep','RepAccountAssignment','Account','MessageSet', 'RepActionType', 'AccountTimeToEngage'), pfizerusdev_learning=c('LearningFile','LearningBuild','LearningRun','AccountTimeToEngageReport','SegmentTimeToEngageReport','TTEhcpReport','TTEsegmentReport'), pfizerusdev_stage=c('AKT_Message_Topic_Email_Learned','Sent_Email_vod__c_arc'), pfizerprod_cs=c('Approved_Document_vod__c','RecordType', 'Call2_Sample_vod__c'))
# LearningRun is for testing, not required for run messageSequenceDriver
resetMockData(homedir,dbuser,dbhost,port,dbpassword,dbname,dbname_cs,requiredMockDataList)

# set required args
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)

# get buildUID
BUILD_UID <<- readModuleConfig(homedir, 'messageTiming','buildUID')
# add mock build dir (func in source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir)))
setupMockBuildDir(homedir, 'messageTiming', BUILD_UID, files_to_copy='learning.properties')

# add entry to learningBuild (simulate what done by API before calling the R script)
config <- initializeConfigurationNew(homedir, BUILD_UID)
VERSION_UID <- config[["versionUID"]]
CONFIG_UID <- config[["configUID"]]

tteParams <- getTTEparams(config, "BUILD")

# run script
source(sprintf("%s/messageTiming/messageTimingData.R", homedir))

dta <- messageTimingData(con, tteParams)

names(dta) <- paste(names(dta),"_new",sep="")

for(i in 1:length(dta)) assign(names(dta)[i], dta[[i]])
# interactions <- dta[[1]]
# accountProduct <- dta[[2]]
# products <- dta[[3]]
# accounts <- dta[[4]]

# disconnect db
dbDisconnect(con)

# test case
test_that("test have correct length of data", {
  expect_equal(length(dta), 4)
  expect_equal(dim(interactions_new),c(48483, 5))
  expect_equal(dim(accountProduct_new),c(560, 42))
  expect_equal(dim(products_new),c(4,2))
})

rm(dta)
test_that("test result is the same as the one saved ", {
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_interactions.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accountProduct.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_products.RData', homedir))
  load(sprintf('%s/messageTiming/tests/data/from_messageTimingData_accounts.RData', homedir))

  expect_equal(interactions_new, interactions)
  expect_equal(products_new, products)
  expect_equal(accounts_new, accounts)

  setkey(accountProduct, accountId, productName)
  setkey(accountProduct_new, accountId, productName)
  accountProduct$productName <- NULL
  accountProduct_new$productName <- NULL
  expect_equal(accountProduct_new, accountProduct)
})
