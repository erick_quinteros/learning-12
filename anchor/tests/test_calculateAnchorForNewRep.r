context('testing calculateAnchorForNewRep() in ANCHOR module')
print(Sys.time())

# load library and source script
library(Learning)
library(properties)
library(futile.logger)
flog.threshold(DEBUG)
# library(data.table)
# setDTthreads(1) # IMPORTANT: to avoid error "OMP: Error #13: Assertion failure at kmp_runtime.cpp(6480)" causing by incompatiblity with data table and forEach
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf("%s/anchor/code/calculateAnchorForNewRep.r",homedir))
source(sprintf("%s/anchor/code/utils.r",homedir))

# load required inputs to functions
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_future.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repAccountAssignments.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_repTeamRep.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateAnchor_result.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateAnchor_newRepList.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocation_result.RData',homedir))
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
predictRundate <- getConfigurationValueNew(config,"LE_AN_nightlyPredictRundate", convertFunc=as.Date)
numberCores <- getConfigurationValueNew(config,"LE_AN_numberCores")
predictAhead <- getConfigurationValueNew(config,"LE_AN_predictAhead") # predict how many days in the future
calAdprobThreshold <- getConfigurationValueNew(config,"LE_AN_calAdprobThreshold", convertFunc=as.numeric, defaultValue=0)
calAdconfThreshold <- getConfigurationValueNew(config,"LE_AN_calAdconfThreshold", convertFunc=as.numeric, defaultValue=0)
predictAheadDayList <- utils.generatePredictAheadDayList(predictRundate, predictAhead)

# run function for calendarAdherenceOn=FALSE
set.seed(1)  # fix that random sampling
calendarAdherenceOn <- FALSE
repCalendarAdherence <- NULL
repAccountCalendarAdherence <- NULL
result_new <- calculateAnchorForNewRep(newRepList, future, repAccountAssignments, repTeamRep, accountDateLocation, result, numberCores, predictRundate, predictAheadDayList, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold, FALSE)
#resultNewRep <- result_new
#save(resultNewRep, file=sprintf("%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep.RData",homedir))
# test cases
test_that("check the result has the correct dimension and same as saved", {
  expect_equal(dim(result_new), c(316,12))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep_calAdOff.RData', homedir))
  #resultNewRep <- result_new
  #save(resultNewRep, file=sprintf('%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep_calAdOff.RData', homedir))
  expect_equal(result_new,resultNewRep)
})

# run function for calendarAdherenceOn=TRUE
set.seed(1)  # fix that random sampling
calendarAdherenceOn <- TRUE
load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repCalendarAdherence.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repAccountCalendarAdherence.RData', homedir))
result_new <- calculateAnchorForNewRep(newRepList, future, repAccountAssignments, repTeamRep, accountDateLocation, result, numberCores, predictRundate, predictAheadDayList, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold, FALSE)
#resultNewRep <- result_new
#save(resultNewRep, file=sprintf("%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep.RData",homedir))
# test cases
test_that("check the result has the correct dimension and same as saved with calendarAdherenceOn=TRUE", {
  expect_equal(dim(result_new), c(316,12))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchorForNewRep_resultNewRep.RData', homedir))
  expect_equal(result_new,resultNewRep)
})