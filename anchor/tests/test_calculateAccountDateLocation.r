context('testing calculateAccountDateLocation script in ANCHOR module')
print(Sys.time())

library(Learning)
library(data.table)
library(futile.logger)
flog.threshold(DEBUG)

# load library and source script
source(sprintf("%s/anchor/code/calculateAccountDateLocation.r",homedir))
source(sprintf("%s/anchor/code/saveAnchorResult.r",homedir))
source(sprintf("%s/anchor/code/utils.r",homedir))

# test calculateAccountDateLocationScores
# load required input data
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_INTERACTIONS_ACCOUNT.RData', homedir))
accountDateLocationScores_new <- calculateAccountDateLocationScores(INTERACTIONS_ACCOUNT)
#accountDateLocationScores <- accountDateLocationScores_new
#save(accountDateLocationScores, file=sprintf('%s/anchor/tests/data/from_calculateAccountDateLocationScores_result.RData',homedir))
test_that("test calculated accountDateLocationScores has the right dimension and same as saved", {
  # test dimension
  expect_equal(dim(accountDateLocationScores_new),c(3661,8))
  # test same as saved
  load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocationScores_result.RData',homedir))
  expect_equal(accountDateLocationScores_new,accountDateLocationScores)
})

# test calculateAccountDateLocation
# load required input data
load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocationScores_result.RData',homedir))
set.seed(1)  # fix that random sampling
accountDateLocation_new <- calculateAccountDateLocation(accountDateLocationScores, FALSE)
accountDateLocation_new_2 <- calculateAccountDateLocation(accountDateLocationScores_new, FALSE)

#accountDateLocation <- accountDateLocation_new
#save(accountDateLocation, file=sprintf('%s/anchor/tests/data/from_calculateAccountDateLocation_result.RData',homedir))
test_that("test calculated accountDateLocation has the right dimension and same as saved", {
  # test dimension
  expect_equal(dim(accountDateLocation_new),c(1922,6))
  # test same as saved
  load(sprintf('%s/anchor/tests/data/from_calculateAccountDateLocation_result.RData',homedir))
  expect_equal(accountDateLocation_new,accountDateLocation)
})
