
from pyspark.context import SparkContext
import string
from utils import Utils

class Data_loading:
    def __init__(self, glue_context, customer, environment, df_load_pandas,df_map_pandas):
        self.glueContext = glue_context
        self.customer = customer
        self.environment = environment
        self.utils = Utils(glue_context, customer)
        self.spark = glue_context.spark_session
        self.df_map_pandas = df_map_pandas
        self.df_load_pandas = df_load_pandas

    def sql_to_template(self, sqlcommand, values):
        '''
        Templated sqlstring to be replaced by the values
        :param sqlcommand:
        :param values:
        :return:
        '''

        template = string.Template('%s' % (sqlcommand))
        sqlstr = template.substitute(values);
        sqlstr = sqlstr.replace('\n', ' ');
        sqlstr = sqlstr.strip()
        return sqlstr




    def get_column_mapping(self, df_map_pandas, values_database):
        '''
        Reads mapping dictionary and populates the mapping values for customer
        :param df_map_pandas: mapping dictionary from column mapping file
        :param values_database: holds the mapping values for customer
        :param customer: actual customer from config file
        :return:  mapping values for customer
        '''
        for index, row in df_map_pandas.iterrows():
            if row['Customer_name'] == self.customer:
                if len(row['Masked_name']) != 0:
                    name_split = row['Customer_attr_watermark'].split(' ')
                    name_split.append(name_split.pop())
                    #print(row['Masked_name'], ' '.join(each for each in name_split))
                    values_database[row['Masked_name']] = ' '.join(each for each in name_split)

        return values_database


    def preparesql(self, df_load_pandas, values_database, tablename):
        '''

        :param date_time:
        :param region:
        :param customer:
        :param environment:
        :param glueContext:
        :param df_load_pandas:
        :param values_database:
        :param tablename:
        :return:
        '''
        partition_columns_list = []
        for index, row in df_load_pandas.iterrows():
            # print(row['table_name'],tablename)
            if row['table_name'] == tablename:
                delta_col = row['delta_column']
                partition_columns_list = row['parition_keys_columns'].split(',')
                print("tablename is matched -->", tablename)
                values_database['otherfilter'] = row['full']
                print("Below is the Sql Query for table Name", tablename)
                sql_query = self.sql_to_template(row['sql_query'], values_database)
                print(sql_query, partition_columns_list)
                return sql_query, partition_columns_list


    # derive a strategy for empty tables from reporting, similar to null columns in the mapping
    def load_tables_from_s3(self,tables, source_s3_location, archive_folder):
        '''
        :param tables:
        :param source_s3_location:
        :param environment:
        :param archive_folder:
        :return:
        '''
        ##if table is empty. we need to have basic structure ready with empty tables]
        # strategy_tables=['strategytarget','targetsperiod','targetinglevel']
        # tables_not_present=['message_topic_email_learned']
        # preuat -pfizerus
        # uat lillyus
        # prod - novsrtisus
        tables_found = []
        tables_not_found = []
        for table in tables:
            if table == 'suggestions':
                table = 'rpt_suggestion_delivered_stg'
                path = 's3://aktana-bdp-adl/'+self.customer+'/prod/suggestions/'
            else:
                path = source_s3_location + self.environment + "/data/" + archive_folder + "/" + table
            print(path)
            print("Loading the table from Parquet")
            df = self.utils.data_load_parquet(self.glueContext, path)
            if df.rdd.isEmpty():
                print(table, "Table does not exist")
                tables_not_found.append(table)
            else:
                df.createOrReplaceTempView(table)
                print(table, " view has been created")
                tables_found.append(table)

        return tables_found, tables_not_found

    # def run (self, source_s3_location, s3_destination,archive_folder, bronze_tables, tables):
    #     print("The data loading has been started from S3")
    #     tables_found, tables_not_found = self.load_tables_from_s3(tables, source_s3_location, archive_folder)
    #     print(tables_found, "These are the tables in memory.")
    #     print(tables_not_found, "These are the tables not found in memory")
    #
    #     # validating the sql queries from data loading file and prep data to write them in bronze area.
    #     values_database = {}
    #     for tablename in bronze_tables:
    #         values_database = self.get_column_mapping(self.df_map_pandas, values_database)
    #         result_sql, partition_columns_list = self.preparesql(self.df_load_pandas, values_database, tablename)
    #         if partition_columns_list[0] !="":
    #             self.utils.df_topartition_delta(self.spark.sql(result_sql), partition_columns_list,
    #                                    s3_destination + "data/bronze/" + tablename)
    #         else: self.utils.df_to_delta(self.spark.sql(result_sql), s3_destination + "data/bronze/" + tablename)
    #
    #         # print(spark.sql(result_sql))





