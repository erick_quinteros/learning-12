##########################################################
#
#
# aktana- copy builds from s3
#
# description: copy builds from s3 for use on emr
#
# created by : learning-dev@aktana.com
#
# created on : 2019-06-05
#
# Copyright AKTANA (c) 2019.
#
#
####################################################################################################################
library(futile.logger)
library(properties)
library(jsonlite)

###########################
## utils funcs
###########################
# function to copy buildfolder
copyBuildFolderFromS3 <- function(localBuildDirPath, s3BuildDirPath, BUILD_UID) {
  flog.info("copy build folder <%s> from s3", BUILD_UID)
  localBuildFilesPath <- sprintf("%s/%s", localBuildDirPath, BUILD_UID)
  s3BuildFilesPath <- sprintf("%s/%s", s3BuildDirPath, BUILD_UID)
  # copy from s3
  shellCode <- sprintf("aws s3 cp %s %s --recursive --exclude *.txt --exclude *.pdf --exclude */messageScoreDriver.r_*.xlsx --exclude messageScoreDriver.r_*.xlsx --quiet", s3BuildFilesPath, localBuildFilesPath)
  system(shellCode)
}

# function to copy nightlyfolder
copyNightlyBuildFolderFromS3 <- function(localBuildDirPath, s3BuildDirPath, nightlyConfigUID) {
  copyBuildFolderFromS3(localBuildDirPath, s3BuildDirPath, sprintf("nightly/%s", nightlyConfigUID))
}

# function to read buildUID needed to be used for nightly config
readBuildUIDFromNightlyPropertiesFile <- function(localBuildDirPath, nightlyConfigUID) {
  propertiesFilepath <- sprintf("%s/nightly/%s/learning.properties", localBuildDirPath, nightlyConfigUID)
  if (!file.exists(propertiesFilepath)) { stop(sprintf("nightlyConfig:%s is missing in s3", nightlyConfigUID)) }
  return(read.properties(propertiesFilepath)[["buildUID"]])
}

# function to copy requried nightly & build folder given nightly configUID
copyNighltyRequiredBuilds <- function(localBuildDirPath, s3BuildDirPath, nightlyConfigUIDs=NULL) {

  if (is.null(nightlyConfigUIDs)) {
    flog.info("not copy anything from s3")

  } else if (length(nightlyConfigUIDs)>0) {

    if (nightlyConfigUIDs[1]=="all") {
      flog.info("copy the whole build folder from s3")
      copyBuildFolderFromS3(localBuildDirPath, s3BuildDirPath, "")

    } else {
      flog.info("The following nighlty config will be copied: %s", paste(nightlyConfigUIDs, collapse=","))
      for (nightlyConfigUID in nightlyConfigUIDs) {
        flog.info("copy for nightly config: %s", nightlyConfigUID)
        # copy required nightly folder
        copyNightlyBuildFolderFromS3(localBuildDirPath, s3BuildDirPath, nightlyConfigUID)
        # read nightly config learning.properties for builds to copy from s3
        requiredBuildUID <- readBuildUIDFromNightlyPropertiesFile(localBuildDirPath, nightlyConfigUID)
        # copy requried build folder
        copyBuildFolderFromS3(localBuildDirPath, s3BuildDirPath, requiredBuildUID)
      }
    }

  } else {
    flog.error("no valid nightlyConfigUIDs for the nightly job")
    stop("no valid nightlyConfigUIDs for the nightly job")
  }

}

############################################################
## Main script
############################################################
# set parameters
args <- commandArgs(T)

if(length(args)==0){
  print("No arguments supplied.")
  if(batch)quit(save = "no", status = 1, runLast = FALSE)
} else {
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}
# config port param to be compatibale to be passed into dbConnect
if (!exists('port')) {
  port <- 0
} else {
  port <- as.numeric(port)
}

# check if nightly run
isNightly <- F
if(!exists("BUILD_UID")) isNightly <- T

# make sure runmodel exists
if(!exists("runmodel")) runmodel <- ""
# check if paraRun, then use rundriver as runmodel
if (runmodel=="paraRunDriver") {
  if(exists("rundriver")) {
    runmodel <- rundriver
  } else {
    stop("rundriver must be provided if using paraRun module")
  }
}

# s3, build path common part
localBuildDirPath <- sprintf("%s/builds", normalizePath(homedir))
targetS3ConfigFilePath <- sprintf("%s/common/sparkUtils/s3Config.json", homedir)
if (file.exists(targetS3ConfigFilePath)) {
  s3BuildDirPath <- fromJSON(targetS3ConfigFilePath)[["s3BuildDirPath"]]
  flog.info("s3BuildDirPath is: %s", s3BuildDirPath)
} else {
  stop(sprintf("%s not exists, failed to get s3 builds path", targetS3ConfigFilePath))
}
# s3BuildDirPath <- sprintf("s3://aktana-bdpus-kafka/learningdata/%s/%s", customer, envname)

# setup db connection
source(sprintf("%s/common/dbConnection/dbConnection.R",homedir))
con <- getDBConnection(dbuser, dbpassword, dbhost, dbname, port)
# register error handler
cleanUp <- function () {
  dbDisconnect(con)
  q('no',status=1) }
options(error=cleanUp)

# copy from s3
if (!isNightly) {
  flog.info("isNightly=FALSE, copy build folder:%s from s3", BUILD_UID)
  copyBuildFolderFromS3(localBuildDirPath, s3BuildDirPath, BUILD_UID)
} else {
  flog.info("isNightly=TRUE, determining which builds needed for %s...", runmodel)
  nightlyConfigUIDs <- NULL

  # get nightlyConfigUIDs needed to determine which builds needed
  if (runmodel=="anchorDriver") {
    nightlyConfigUIDs <- c("AKT_ANCHOR_V0")

  } else if (runmodel=="engagementDriver") {
    nightlyConfigUIDs <- c("AKT_REM_V0")

  } else if (runmodel=="messageScoreDriver" | runmodel=="NightlySimulationDriver" | runmodel=="sparkMessageScoreDriver") {
    # get messageAlgorithmIds from DSE table MessageAlgorithm and MessageSet
    nightlyConfigUIDs <- dbGetQuery(con,"SELECT distinct ma.externalId FROM MessageSet ms JOIN (SELECT messageAlgorithmId, externalId FROM MessageAlgorithm where isDeleted=0 and isActive=1 and messageAlgorithmType=1) ma ON ms.messageAlgorithmId=ma.messageAlgorithmId;")$externalId

  } else if (runmodel=="messageTimingScoreDriver") {
    # get messageAlgorithmIds from DSE table MessageAlgorithm and MessageSet
    nightlyConfigUIDs <- dbGetQuery(con,"Select distinct externalId from TimeToEngageAlgorithm where isDeleted=0 and isActive=1")$externalId
  
  } else if (runmodel=="AnchorAccuracyReportDriver" | runmodel=="MSOPredictorOptimizer" | runmodel=="TTEPredictorOptimizer" | runmodel=="anchorOldDriver" | runmodel=="RepEngagementCalculator" | runmodel=="syncEventTypeDriver") {
    # anchor accuracy & MSO Predictor Optimizer module do not need build folder
    nightlyConfigUIDs <- NULL
    
  } else if (runmodel=="messageSequenceDriver" | runmodel=="messageTimingDriver") {
    flog.error("BUILD_UID is required for %s", runmodel)
    nightlyConfigUIDs <- NULL
    stop(sprintf("BUILD_UID is required for %s", runmodel))

  } else if (runmodel=="all") {
    flog.info("forced to copy the whole builds folder from s3")
    nightlyConfigUIDs <- c("all")
    
  } else {
    flog.error("driver:%s is not supported now", runmodel)
    nightlyConfigUIDs <- NULL
    stop(sprintf("driver:%s is not supported now", runmodel))

  }

  # call to copy builds from s3
  copyNighltyRequiredBuilds(localBuildDirPath, s3BuildDirPath, nightlyConfigUIDs)
}

flog.info("finish copying build folders from s3")

# save copy finish time for later use to determine which files are new files
copyFinishTime <- Sys.time()
copyFinishTimeSavePath <- sprintf("%s/copyFinishTime.RData", logdir)
save(copyFinishTime, file=copyFinishTimeSavePath)
flog.info("finish saving copy completion timestamp for later use")

# diconnect db
dbDisconnect(con)
