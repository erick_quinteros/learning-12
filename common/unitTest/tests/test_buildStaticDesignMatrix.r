context('testing buildStaticDesignMatrix from learningPackage')
print(Sys.time())

# load packages and scripts needed for running tests
source(sprintf('%s/common/unitTest/customExpectFunc.r',homedir))
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf('%s/common/unitTest/utilsFunc.r',homedir))

# loading Learning package to call function for test
library(Learning)

# set up log file to test log contents
fileName <- sprintf('%s/common/unitTest/tests/log_buildStaticDesignMatrix.txt',homedir)
if (file.exists(fileName)) file.remove(fileName)
flog.appender(appender.file(fileName))

# load required input
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_accountProduct.RData', homedir))
load(sprintf('%s/messageSequence/tests/data/from_loadMessageSequenceData_emailTopicNames.RData', homedir))
prods <- strsplit(readModuleConfig(homedir, 'common/unitTest','prods'),";")[[1]]
# test fucntion with logprocess logDropProcess=TRUE and removeSpecialCharacterInAP=FALSE
result <- buildStaticDesignMatrix(prods, accountProduct, emailTopicNames, logDropProcess=TRUE)
print(result[["colsNotDcast"]])

test_that("result has the correct dimension and are the same as saved", {
  expect_length(result,6)
  expect_equal(dim(result[["AP"]]), c(309,606))
  expect_equal(dim(result[["APpredictors"]]), c(204,3))
  expect_length(result[["predictorNamesAPColMap"]], 533)
  expect_equal(result[["colsForDcast"]], character())
  expect_equal(result[["needFurtherDcastInChunks"]], FALSE)
  expect_equal(result[["chunkSize"]], 309)
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_AP.RData',homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_APpredictors.RData',homedir))
  load(sprintf('%s/common/unitTest/data/from_buildStaticDesignMatrix_predictorNamesAPColMap.RData',homedir))
  expect_equal(result[["AP"]][,colnames(AP),with=F], AP)
  expect_equal(result[["APpredictors"]], APpredictors)
  expect_equal(result[["predictorNamesAPColMap"]][order(unlist(result[["predictorNamesAPColMap"]]))], predictorNamesAPColMap[order(unlist(predictorNamesAPColMap))])
})

test_that("check log file contents", {
  log_contents <- readLogFileBase(fileName)
  expect_str_start_match(log_contents[3],'151 of 204')  # number of predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique value
  expect_str_start_match(log_contents[4],'1 of 21 numeric predictors (53 all predictors)')  # number of numeric predictors from AccountProduct/Account dropped before loop to build models as there are either less than 5 unique values or having <2 quantile >0
  expect_str_start_match(log_contents[5],'0 of 52')  # number of remaining predictors from AccountProduct/Account dropped before loop to build models as they have less than 2 unique values (2nd check after processing numeric and character variable separately
  expect_str_end_match(log_contents[6],'52 to 533')  # check for dcast manupilation
  expect_str_end_match(log_contents[7],'added 72 and becomes 605') # check EmailTopic Open Rates predictor added
})

flog.appender(appender.console())
file.remove(fileName)
