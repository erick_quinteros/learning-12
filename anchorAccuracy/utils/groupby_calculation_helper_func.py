import pandas as pd
import numpy as np
from .math_helper_func import haversine_numpy, reject_outliers, intersection_area, get_max_distance_between_n_points, lat_lon_centroid


def _calculate_num_of_points(group):
    # num of points in group
    ind_nan_points = np.isnan(group.loc[:, ['latitude', 'longitude']]).any(axis=1)
    num_of_points = sum(~ind_nan_points)
    return ind_nan_points, num_of_points


def _calcualte_centroid(group):
    centroid_latitude, centroid_longitude = lat_lon_centroid(group.loc[:, ['latitude', 'longitude']].values)
    return centroid_latitude, centroid_longitude


def _calculate_distance_between_one_fixed_point_to_visit_points(group, ind_nan_points, point_latitude, point_longitude):
    # calculate radius as max distance from centroid
    # prepare the latitude longitude array
    lat_lon_1 = group.loc[~ind_nan_points, ['latitude', 'longitude']].values
    lat_lon_2 = np.tile(np.array([point_latitude, point_longitude]), (lat_lon_1.shape[0], 1))
    lat_lon_arr = np.concatenate((lat_lon_1, lat_lon_2), axis=1)
    # calculate the distance based on the latitude and longitude
    distance_array = haversine_numpy(lat_lon_arr)
    return distance_array


def _calculate_max_day_travel(group, ind_nan_points):
    # calculate the max distance between two points
    # get points used for create the permutation
    visit_point_to_consider = group.loc[~ind_nan_points, ['latitude', 'longitude']].values.tolist()
    max_visit_distance = get_max_distance_between_n_points(visit_point_to_consider)
    return max_visit_distance


def calculate_basic_metrics_visit_predict(group):
    """
    group by helper funcs to help calculate radius, centroid, max daily travel distance of visit, and number of visits and number of visit in visit-predict overlap,
    :param group:
    :return:
    """
    # num of points in group
    ind_nan_visit, num_of_visits = _calculate_num_of_points(group)

    # prepare predict_latitude, predict_longitude for rewrite out
    predict_latitude = group['predict_latitude'].values[0]
    predict_longitude = group['predict_longitude'].values[0]

    # prepare maxNearDistance for rewrite out
    max_near_distance = group['maxNearDistance'].values[0]
    if ~pd.isnull(max_near_distance):
        max_near_distance = max_near_distance/1000

    # check nan, if all nan, pass
    if num_of_visits < 1:  # no non-nan record
        centroid_latitude, centroid_longitude, num_of_visits_in_overlap, max_visit_distance, radius, predicted_visited_mse = np.nan, np.nan, np.nan, np.nan, np.nan, np.nan
    else:
        # calculate centroid
        centroid_latitude, centroid_longitude = _calcualte_centroid(group)

        # calculate the distance between visit centroid to visit points
        visit_centroid_distance_array = _calculate_distance_between_one_fixed_point_to_visit_points(group, ind_nan_visit, centroid_latitude, centroid_longitude)
        # calculate radius as max distance from centroid
        radius = np.nanmax(visit_centroid_distance_array)

        # calculate the max distance between two points
        max_visit_distance = _calculate_max_day_travel(group, ind_nan_visit)

        if np.isnan(predict_latitude) or np.isnan(predict_longitude):
            num_of_visits_in_overlap, predicted_visited_mse = 0, np.nan
        else:
            # calculate the distance between predict centorid to visit points
            predict_centroid_distance_array = _calculate_distance_between_one_fixed_point_to_visit_points(group, ind_nan_visit, predict_latitude, predict_longitude)

            # calculate predictedVisitedMSE
            predicted_visited_mse = np.sum(np.square(predict_centroid_distance_array) - np.square(visit_centroid_distance_array)) / num_of_visits

            # calcualte number of visited points within the predict circle
            num_of_visits_in_overlap = sum(predict_centroid_distance_array <= radius)

    return pd.Series(
        [predict_latitude, predict_longitude, centroid_latitude, centroid_longitude, num_of_visits,
         num_of_visits_in_overlap, max_visit_distance, radius, max_near_distance, predicted_visited_mse],
        index=['predict_latitude', 'predict_longitude', 'centroid_latitude', 'centroid_longitude', 'numOfVisits',
               'numOfVisitsInOverlap', 'maxDayTravelKm', 'visitRadiusKm', 'maxNearDistanceKm', 'predictedVisitedMSE'])


def calculate_predict_coverage(x):
    overlap_area = x.loc['overlapAreaKm2']
    visit_area = x.loc['visitAreaKm2']
    distance = x.loc['predictedVisitedDiffKm']
    if np.isnan(visit_area):
        # no visit
        return np.nan
    elif visit_area == 0:
        if np.isnan(distance) or distance > 0:
            return 0
        else:
            return 1
    else:
        return overlap_area / visit_area


def calculate_percentage_visits_in_overlap(x):
    n_visits = x.loc['numOfVisits']
    n_overlap = x.loc['numOfVisitsInOverlap']
    if n_visits == 0:
        return np.nan
    else:
        return n_overlap / n_visits


def calculate_suggesstion_in_predict_circle(group):
    # calculate number of suggestion
    num_of_sugs = sum(~np.isnan(group.loc[:,['latitude','longitude']]).any(axis=1))
    no_predict = np.isnan(group['predict_latitude'].values[0]) or np.isnan(group['predict_longitude'].values[0])
    no_radius = np.isnan(group['visitRadiusKm'].values[0])
    if num_of_sugs==0:
        num_sugs_within = np.nan
    elif no_predict or no_radius: # only have suggest record
        num_sugs_within = 0
    else:
        num_sugs_within = sum(haversine_numpy(group.loc[:,['latitude','longitude','predict_latitude','predict_longitude']].values) <= group['visitRadiusKm'].values[0])
    return pd.Series([num_of_sugs, num_sugs_within],index=['numOfSugs','numOfSugsInPredict'])


def prepare_set_tuple(group):
    non_nan_ind = ~(pd.isnull(group.loc[:, ['latitude', 'longitude']]).any(axis=1))
    return set(tuple(map(tuple, group.loc[non_nan_ind, ['latitude', 'longitude']].values.tolist())))


def calculate_max_travel_over_max_near_distance(x):
    max_day_travel = x.loc['maxDayTravelKm']
    max_near_distance = x.loc['maxNearDistanceKm']
    if pd.isnull(max_near_distance):
        return np.nan
    elif max_near_distance == 0:
        return 0
    else:
        return max_day_travel / (max_near_distance)


def calculate_max_daily_travel(group):
    ind_nan_visit, num_of_visits = _calculate_num_of_points(group)
    max_visit_distance = _calculate_max_day_travel(group, ind_nan_visit)
    return pd.Series([max_visit_distance], index=['max_visit_distance'])


def calculate_max_near_distance(group):
    max_day_travel = group['maxDayTravelKm'].values
    max_day_travel = max_day_travel[~np.isnan(max_day_travel)]

    if max_day_travel.shape[0]>0: # not all nan
        maxFarDistance = np.quantile(max_day_travel, 0.75)
        maxNearDistance = max_day_travel[max_day_travel <= maxFarDistance].mean()
    else:
        maxNearDistance = np.nan

    return maxNearDistance
